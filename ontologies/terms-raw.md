### Possible Ontology Term Mapping for Companies House Data

Companies House Fields taken from:

http://resources.companieshouse.gov.uk/toolsToHelp/pdf/freeDataProductDataset.pdf

(The first 9 entries aren't explicit fields, but sections in the CH fields)

| Companies House Field                     | Max. size |
|-------------------------------------------|-----------|
| *Registered Office Address*               |           |
| *Accounts*                                |           |
| *Returns*                                 |           |
| *Mortgages*                               |           |
| *SIC Codes*                               | (occurs max 4) |
| *Limited Partnerships*                    |           |
| *Previous Names*                          | (occurs max 10) |
| *Confirmation*                            |           |
| *Statement*                               |           |
|                                           |           |
| CompanyName                               | 160       |
| CompanyNumber                             | 8         |
| Careof                                    | 100       |
| POBox                                     | 10        |
| AddressLine1(HouseNumber and Street)      | 300       |
| AddressLine2(area)                        | 300       |
| PostTown                                  | 50        |
| County(region)                            | 50        |
| Country                                   | 50        |
| PostCode                                  | 20        |
| CompanyCategory(corporate_body_type_desc) | 100       |
| CompanyStatus(action_code_desc)           | 70        |
| CountryofOrigin                           | 50        |
| Dissolution Date                          | 10        |
| IncorporationDate                         | 10        |
| AccountingRefDay                          | 2         |
| AccountingRefMonth                        | 2         |
| NextDueDate                               | 10        |
| LastMadeUpDate                            | 10        |
| AccountsCategory(accounts_type_desc)      | 30        |
| NextDueDate                               | 10        |
| LastMadeUpDate                            | 10        |
| NumMortCharges                            | 6         |
| NumMortOutstanding                        | 6         |
| NumMortPartSatisfied                      | 6         |
| NumMortSatisfied                          | 6         |
| SICCode1                                  | 170       |
| SICCode2                                  | 170       |
| SICCode3                                  | 170       |
| SICCode4                                  | 170       |
| NumGenPartners                            | 6         |
| NumLimPartners                            | 6         |
| URI                                       | 47        |
| Change of Name Date                       | 10        |
| Company name (previous)                   | 160       |
| ConfStmtNextDueDate                       | 10        |
| ConfStmtLastMadeUpDate                    | 10        |
